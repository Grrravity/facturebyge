import 'package:flutter/material.dart';

Color getBlackOrWhiteResponsiveColor(Color color) {
  return ThemeData.estimateBrightnessForColor(color) == Brightness.light
      ? Colors.black
      : Colors.white;
}
