import 'package:facture_by_ge/Bloc/billing/billing_view_controller.dart';
import 'package:facture_by_ge/Bloc/client/client_view_controller.dart';
import 'package:facture_by_ge/Bloc/project/project_view_controller.dart';
import 'package:facture_by_ge/Bloc/repo/repo_view_controller.dart';
import 'package:facture_by_ge/Screen/Layout/skeleton.dart';
import 'package:facture_by_ge/Screen/billing_view.dart';
import 'package:facture_by_ge/Screen/client_view.dart';
import 'package:facture_by_ge/Screen/project_view.dart';
import 'package:facture_by_ge/Screen/repo_view.dart';
import 'package:get/get.dart';

class Routes {
  static String get initialRoute => billing;

  static String error404 = '/404';
  static const String home = '/';
  static const String billing = '/bill';
  static const String client = '/client';
  static const String project = '/project';
  static const String repo = '/repository';
}

class Navigate {
  static List<GetPage> routes = [
    GetPage(
      name: Routes.home,
      page: () => MainSkeleton(currentIndex: 0, body: BillingView()),
      binding: BindingsBuilder(() => {Get.put(BillingViewController())}),
    ),
    GetPage(
      name: Routes.billing,
      page: () => BillingView(),
      binding: BindingsBuilder(() => {Get.put(BillingViewController())}),
    ),
    GetPage(
      name: Routes.client,
      page: () => ClientView(),
      binding: BindingsBuilder(() => {Get.put(ClientViewController())}),
    ),

    GetPage(
      name: Routes.project,
      page: () => ProjectView(),
      binding: BindingsBuilder(() => {Get.put(ProjectViewController())}),
    ),

    GetPage(
      name: Routes.repo,
      page: () => RepoView(),
      binding: BindingsBuilder(() => {Get.put(RepoViewController())}),
    ),

    ///GetPage(
    ///  name: Routes.client,
    ///  page: () => ClientView(),
    ///  binding: ClientViewControllerBuilding(),
    ///),
    ///GetPage(
    ///  name: Routes.project,
    ///  page: () => ProjectView(),
    ///  binding: ProjectViewControllerBuilding(),
    ///),
    ///GetPage(
    ///  name: Routes.repo,
    ///  page: () => RepoView(),
    ///  binding: RepoViewControllerBuilding(),
    ///),
    //subroute
    //GetPage(
    //  title: 'products'.tr.capitalizeFirst,
    //  name: Routes.familles +
    //      Routes.edit +
    //      '/:familyId' +
    //      Routes.produits +
    //      Routes.create,
    //  page: () => web.ProductView(),
    //  binding: web.ProductViewControllerBindings(),
    //),

    //Route avec des sous routes bis
    //GetPage(
    //  title: 'equipment'.tr.capitalizeFirst,
    //  name: Routes.modelsEquipements,
    //  page: () => web.EquipementsView(),
    //  binding: web.EquipmentViewControllerBindings(),
    //  children: [
    //    GetPage(
    //      name: Routes.equipementsModelsCreate,
    //      page: () => web.TechnicalDataModelsView(),
    //      binding: web.TechnicalDataModelsViewControllerBindings(),
    //    ),
    //    GetPage(
    //      name: Routes.equipementsModelsEdit,
    //      page: () => web.TechnicalDataModelsView(),
    //      binding: web.TechnicalDataModelsViewControllerBindings(),
    //    ),
    //    GetPage(
    //      name: Routes.equipements,
    //      page: () => web.ListingEquipementsView(),
    //      binding: web.ListingEquipementsViewControllerBindings(),
    //      children: [
    //        GetPage(
    //          name: Routes.equipementsCreate,
    //          page: () => web.CreateEquipementsView(),
    //          binding: web.CreateEquipmentViewControllerBindings(),
    //        ),
    //        GetPage(
    //          name: Routes.equipementsEdit,
    //          page: () => web.EditEquipementsView(),
    //          binding: web.EditEquipmentViewControllerBindings(),
    //        ),
    //      ],
    //    ),
    //  ],
    //),
  ];

  static GetPage unknown = GetPage(
    name: Routes.error404,
    page: () => BillingView(),
    binding: BindingsBuilder(() => {Get.put(BillingViewController())}),
  );
}
