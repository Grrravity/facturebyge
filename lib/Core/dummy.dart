import 'package:facture_by_ge/Core/hive_box.dart';
import 'package:facture_by_ge/Repository/bill/bill.dart';
import 'package:facture_by_ge/Repository/client/client.dart';
import 'package:facture_by_ge/Repository/commit/commit.dart';
import 'package:facture_by_ge/Repository/freelance/freelance.dart';
import 'package:facture_by_ge/Repository/payment/payment.dart';
import 'package:facture_by_ge/Repository/pricing/pricing.dart';
import 'package:facture_by_ge/Repository/project/project.dart';
import 'package:facture_by_ge/Repository/task/task.dart';
import 'package:hive/hive.dart';

generateData() async {
  Freelance freelance = Freelance.dummy();

  var freelanceBox = await Hive.openBox<Freelance>(BoxKey.freelance);
  freelanceBox.put(freelance.id, freelance);

  var clientBox = await Hive.openBox<Client>(BoxKey.client);
  var billBox = await Hive.openBox<Bill>(BoxKey.bill);
  var projectBox = await Hive.openBox<Project>(BoxKey.project);
  var paymentBox = await Hive.openBox<Payment>(BoxKey.payment);
  var taskBox = await Hive.openBox<Task>(BoxKey.task);
  var commitBox = await Hive.openBox<Commit>(BoxKey.commit);
  var pricingBox = await Hive.openBox<Pricing>(BoxKey.pricing);

  Map<int, Client> _clients = {};
  Map<int, Bill> _bills = {};
  Map<int, Project> _projects = {};
  Map<int, Payment> _payments = {};
  Map<int, Task> _tasks = {};
  Map<int, Commit> _commits = {};
  Map<int, Pricing> _pricings = {};

  if (freelance.clients != null) {
    for (var client in freelance.clients!) {
      _clients.addAll({client.id: client});

      if (client.bills != null) {
        for (var bill in client.bills!) {
          _bills.addAll({bill.id: bill});

          if (bill.payments != null) {
            for (var payment in bill.payments!) {
              _payments.addAll({payment.id: payment});
            }
          }
          if (bill.regulations != null) {
            for (var regulation in bill.regulations!) {
              _payments.addAll({regulation.id: regulation});
            }
          }
        }
      }
      if (client.projects != null) {
        for (var project in client.projects!) {
          _projects.addAll({project.id: project});

          if (project.tasks != null) {
            for (var task in project.tasks!) {
              _tasks.addAll({task.id: task});
              _pricings.addAll({task.pricing.id: task.pricing});

              if (task.commits != null) {
                for (var commit in task.commits!) {
                  _commits.addAll({commit.id: commit});
                }
              }
            }
          }
        }
      }
    }
  }
  clientBox.putAll(_clients);
  billBox.putAll(_bills);
  projectBox.putAll(_projects);
  paymentBox.putAll(_payments);
  taskBox.putAll(_tasks);
  commitBox.putAll(_commits);
  pricingBox.putAll(_pricings);
}
