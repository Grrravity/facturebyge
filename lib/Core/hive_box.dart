class BoxKey {
  static const String freelance = 'FREELANCE';
  static const String bill = 'BILL';
  static const String client = 'CLIENT';
  static const String project = 'PROJECT';
  static const String payment = 'PAYMENT';
  static const String pricing = 'PRICING';
  static const String commit = 'COMMIT';
  static const String tarif = 'TARIF';
  static const String repo = 'REPO';
  static const String task = 'TASK';
  static const String user = 'USER';
  static const String jwt = 'JWT';
}
