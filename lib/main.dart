import 'package:facture_by_ge/Core/dummy.dart';
import 'package:facture_by_ge/Core/routes.dart';
import 'package:facture_by_ge/Repository/adapter_register.dart';
import 'package:facture_by_ge/Screen/billing_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/date_symbol_data_local.dart';

void main() async {
  await Hive.initFlutter();
  await registerHiveAdapters();
  await generateData();
  await initializeDateFormatting('fr_FR', null);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      defaultTransition: Transition.fadeIn,
      unknownRoute: Navigate.unknown,
      initialRoute: Routes.initialRoute,
      getPages: Navigate.routes,
      debugShowCheckedModeBanner: false,
      title: 'Factures by GE',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BillingView(),
    );
  }
}
