import 'package:facture_by_ge/Repository/client/client.dart';
import 'package:hive/hive.dart';

part 'freelance.g.dart';

@HiveType(typeId: 2)
class Freelance extends HiveObject {
  @HiveField(0)
  int id;
  @HiveField(1)
  String companyName;
  @HiveField(2)
  String address;
  @HiveField(3)
  String city;
  @HiveField(4)
  String zip;
  @HiveField(5)
  String country;
  @HiveField(6)
  String firstName;
  @HiveField(7)
  String lastName;
  @HiveField(8)
  String? siret;
  @HiveField(9)
  String phone;
  @HiveField(10)
  String email;
  @HiveField(11)
  String? siteWeb;
  @HiveField(12)
  String? logoPath;
  @HiveField(13)
  List<Client>? clients;

  Freelance({
    required this.id,
    required this.companyName,
    required this.address,
    required this.city,
    required this.zip,
    required this.country,
    required this.firstName,
    required this.lastName,
    this.siret,
    required this.phone,
    required this.email,
    this.siteWeb,
    this.logoPath,
    this.clients,
  });

  factory Freelance.dummy() {
    return Freelance(
        id: 1,
        companyName: 'GrrravityBusiness',
        address: '1 avenue des champs élysées',
        city: 'Paris',
        zip: '75000',
        country: 'France',
        firstName: 'Jhon',
        lastName: 'Doe',
        siret: '00011122233333',
        phone: '+33 0 00 00 00 00',
        email: 'jhon.doe@gmail.com',
        siteWeb: 'www.jhondoe.com',
        logoPath: 'www.jhondoe.com/monimage',
        clients: [Client.dummy(1), Client.dummy(2), Client.dummy(3)]);
  }

  factory Freelance.fromJson(Map<String, dynamic> json) {
    return Freelance(
      id: json['id'] ?? -1,
      companyName: json['company_name'] ?? "unspecified",
      address: json['address'] ?? "unknown",
      city: json['city'] ?? "unknown",
      zip: json['zip'] ?? "unknown",
      country: json['country'] ?? "unknown",
      firstName: json['first_name'] ?? "unknown",
      lastName: json['last_name'] ?? "unknown",
      siret: json['siret'],
      phone: json['phone'] ?? "+33 0 00 00 00 00",
      email: json['email'] ?? "unknown@unknown.fr",
      siteWeb: json['site_web'],
      logoPath: json['logo_path'],
      clients:
          List<Client>.from(json['clients'].map((x) => Client.fromJson(x))),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['company_name'] = companyName;
    data['address'] = address;
    data['city'] = city;
    data['zip'] = zip;
    data['country'] = country;
    data['first_name'] = firstName;
    data['last_name'] = lastName;
    data['siret'] = siret;
    data['phone'] = phone;
    data['email'] = email;
    data['site_web'] = siteWeb;
    data['logo_path'] = logoPath;
    data['clients'] = clients?.map((v) => v.toJson()).toList();
    return data;
  }
}
