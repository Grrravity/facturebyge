// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pricing.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PricingAdapter extends TypeAdapter<Pricing> {
  @override
  final int typeId = 6;

  @override
  Pricing read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Pricing(
      id: fields[0] as int,
      type: fields[1] as PricingType,
      amount: fields[2] as double,
    );
  }

  @override
  void write(BinaryWriter writer, Pricing obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.type)
      ..writeByte(2)
      ..write(obj.amount);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PricingAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
