import 'dart:math';

import 'package:facture_by_ge/Repository/pricing/pricing_type_enum.dart';
import 'package:hive/hive.dart';

part 'pricing.g.dart';

@HiveType(typeId: 6)
class Pricing extends HiveObject {
  @HiveField(0)
  int id;
  @HiveField(1)
  PricingType type;
  @HiveField(2)
  double amount;

  Pricing({required this.id, required this.type, required this.amount});

  factory Pricing.fromJson(Map<String, dynamic> json) {
    return Pricing(
        id: json['id'] ?? -1,
        type: stringToPricingType(json['type']),
        amount: json['amount'] ?? 0.0);
  }

  factory Pricing.dummy(int id) {
    List<PricingType> pricingtype = [
      PricingType.credit,
      PricingType.time,
      PricingType.discount,
    ];
    Random rng = Random();
    return Pricing(
        id: id, type: pricingtype[rng.nextInt(2)], amount: id + 250.54);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['type'] = pricingTypeToString(type);
    data['amount'] = amount;
    return data;
  }
}
