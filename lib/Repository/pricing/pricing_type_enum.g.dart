// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pricing_type_enum.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PricingTypeAdapter extends TypeAdapter<PricingType> {
  @override
  final int typeId = 7;

  @override
  PricingType read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return PricingType.credit;
      case 1:
        return PricingType.time;
      case 2:
        return PricingType.discount;
      default:
        return PricingType.credit;
    }
  }

  @override
  void write(BinaryWriter writer, PricingType obj) {
    switch (obj) {
      case PricingType.credit:
        writer.writeByte(0);
        break;
      case PricingType.time:
        writer.writeByte(1);
        break;
      case PricingType.discount:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PricingTypeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
