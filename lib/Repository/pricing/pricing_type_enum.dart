import 'package:hive/hive.dart';

part 'pricing_type_enum.g.dart';

@HiveType(typeId: 7)
enum PricingType {
  @HiveField(0)
  credit,

  @HiveField(1)
  time,

  @HiveField(2)
  discount
}
String pricingTypeToString(PricingType value) {
  switch (value) {
    case PricingType.credit:
      return 'credit';
    case PricingType.time:
      return 'time';
    case PricingType.discount:
      return 'discount';
    default:
      return 'credit';
  }
}

PricingType stringToPricingType(String value) {
  switch (value) {
    case 'credit':
      return PricingType.credit;
    case 'time':
      return PricingType.time;
    case 'discount':
      return PricingType.discount;
    default:
      return PricingType.credit;
  }
}
