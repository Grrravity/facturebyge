import 'package:facture_by_ge/Repository/bill/bill.dart';
import 'package:facture_by_ge/Repository/bill/bill_status_enum.dart';
import 'package:facture_by_ge/Repository/client/client.dart';
import 'package:facture_by_ge/Repository/commit/commit.dart';
import 'package:facture_by_ge/Repository/freelance/freelance.dart';
import 'package:facture_by_ge/Repository/payment/payment.dart';
import 'package:facture_by_ge/Repository/payment/payment_type_enum.dart';
import 'package:facture_by_ge/Repository/pricing/pricing.dart';
import 'package:facture_by_ge/Repository/pricing/pricing_type_enum.dart';
import 'package:facture_by_ge/Repository/project/project.dart';
import 'package:facture_by_ge/Repository/task/task.dart';
import 'package:hive/hive.dart';

registerHiveAdapters() {
  Hive.registerAdapter<Bill>(BillAdapter());
  Hive.registerAdapter<Client>(ClientAdapter());
  Hive.registerAdapter<Freelance>(FreelanceAdapter());
  Hive.registerAdapter<Project>(ProjectAdapter());
  Hive.registerAdapter<Task>(TaskAdapter());
  Hive.registerAdapter<Commit>(CommitAdapter());
  Hive.registerAdapter<Pricing>(PricingAdapter());
  Hive.registerAdapter<PricingType>(PricingTypeAdapter());
  Hive.registerAdapter<Payment>(PaymentAdapter());
  Hive.registerAdapter<PaymentType>(PaymentTypeAdapter());
  Hive.registerAdapter<BillStatus>(BillStatusAdapter());
}
