// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bill.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BillAdapter extends TypeAdapter<Bill> {
  @override
  final int typeId = 0;

  @override
  Bill read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Bill(
      id: fields[0] as int,
      dueDate: fields[1] as DateTime?,
      sendDate: fields[2] as DateTime?,
      asTva: fields[3] as bool,
      asPenaltyWarning: fields[4] as bool,
      tvaAmount: fields[5] as double,
      payments: (fields[6] as List?)?.cast<Payment>(),
      regulations: (fields[7] as List?)?.cast<Payment>(),
      status: fields[8] as BillStatus,
      createdAt: fields[9] as DateTime,
      total: fields[10] as double,
      paid: fields[11] as double?,
    );
  }

  @override
  void write(BinaryWriter writer, Bill obj) {
    writer
      ..writeByte(12)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.dueDate)
      ..writeByte(2)
      ..write(obj.sendDate)
      ..writeByte(3)
      ..write(obj.asTva)
      ..writeByte(4)
      ..write(obj.asPenaltyWarning)
      ..writeByte(5)
      ..write(obj.tvaAmount)
      ..writeByte(6)
      ..write(obj.payments)
      ..writeByte(7)
      ..write(obj.regulations)
      ..writeByte(8)
      ..write(obj.status)
      ..writeByte(9)
      ..write(obj.createdAt)
      ..writeByte(10)
      ..write(obj.total)
      ..writeByte(11)
      ..write(obj.paid);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BillAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
