import 'package:facture_by_ge/Core/ui_constants.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

part 'bill_status_enum.g.dart';

@HiveType(typeId: 10)
enum BillStatus {
  @HiveField(0)
  editing,
  @HiveField(1)
  ready,
  @HiveField(2)
  sent,
  @HiveField(3)
  closed,
}
String billStatusToString(BillStatus value) {
  switch (value) {
    case BillStatus.editing:
      return 'édition';
    case BillStatus.ready:
      return 'prête';
    case BillStatus.sent:
      return 'envoyée';
    case BillStatus.closed:
      return 'terminée';
    default:
      return 'édition';
  }
}

BillStatus stringToBillValue(String value) {
  switch (value) {
    case 'édition':
      return BillStatus.editing;
    case 'prête':
      return BillStatus.ready;
    case 'envoyée':
      return BillStatus.sent;
    case 'terminée':
      return BillStatus.closed;
    default:
      return BillStatus.editing;
  }
}

Color billStatusToColor(BillStatus value) {
  switch (value) {
    case BillStatus.editing:
      return UiConstants.secondaryYellow10;
    case BillStatus.ready:
      return UiConstants.primaryBlue10;
    case BillStatus.sent:
      return UiConstants.secondaryGreen10;
    case BillStatus.closed:
      return UiConstants.backgroundLight;
    default:
      return UiConstants.secondaryYellow10;
  }
}

Color billStatusToTextColor(BillStatus value) {
  switch (value) {
    case BillStatus.editing:
      return UiConstants.secondaryYellow;
    case BillStatus.ready:
      return UiConstants.primaryBlue;
    case BillStatus.sent:
      return UiConstants.secondaryGreen;
    case BillStatus.closed:
      return UiConstants.primaryGrey;
    default:
      return UiConstants.secondaryYellow;
  }
}
