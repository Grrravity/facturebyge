// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bill_status_enum.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BillStatusAdapter extends TypeAdapter<BillStatus> {
  @override
  final int typeId = 10;

  @override
  BillStatus read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return BillStatus.editing;
      case 1:
        return BillStatus.ready;
      case 2:
        return BillStatus.sent;
      case 3:
        return BillStatus.closed;
      default:
        return BillStatus.editing;
    }
  }

  @override
  void write(BinaryWriter writer, BillStatus obj) {
    switch (obj) {
      case BillStatus.editing:
        writer.writeByte(0);
        break;
      case BillStatus.ready:
        writer.writeByte(1);
        break;
      case BillStatus.sent:
        writer.writeByte(2);
        break;
      case BillStatus.closed:
        writer.writeByte(3);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BillStatusAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
