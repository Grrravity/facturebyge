import 'dart:math';

import 'package:facture_by_ge/Repository/bill/bill_status_enum.dart';
import 'package:facture_by_ge/Repository/payment/payment.dart';
import 'package:hive/hive.dart';

part 'bill.g.dart';

@HiveType(typeId: 0)
class Bill extends HiveObject {
  @HiveField(0)
  int id;
  @HiveField(1)
  DateTime? dueDate;
  @HiveField(2)
  DateTime? sendDate;
  @HiveField(3)
  bool asTva;
  @HiveField(4)
  bool asPenaltyWarning;
  @HiveField(5)
  double tvaAmount;
  @HiveField(6)
  List<Payment>? payments;
  @HiveField(7)
  List<Payment>? regulations;
  @HiveField(8)
  BillStatus status;
  @HiveField(9)
  DateTime createdAt;
  @HiveField(10)
  double total;
  @HiveField(11)
  double? paid;

  Bill(
      {required this.id,
      this.dueDate,
      this.sendDate,
      required this.asTva,
      required this.asPenaltyWarning,
      required this.tvaAmount,
      this.payments,
      this.regulations,
      required this.status,
      required this.createdAt,
      required this.total,
      this.paid});

  factory Bill.fromJson(Map<String, dynamic> json) {
    return Bill(
        id: json['id'] ?? -1,
        dueDate: DateTime.tryParse(json['due_date']),
        sendDate: DateTime.tryParse(json['send_date']),
        asTva: json['as_tva'] ?? false,
        asPenaltyWarning: json['as_penalty_warning'] ?? true,
        tvaAmount: json['tva_amount'] ?? 12.5,
        payments: List<Payment>.from(
            json['payments'].map((x) => Payment.fromJson(x))),
        regulations: List<Payment>.from(
            json['regulations'].map((x) => Payment.fromJson(x))),
        status: stringToBillValue(json['status']),
        createdAt: DateTime.tryParse(json['created_at']) ?? DateTime.now(),
        total: json['total'] ?? 0.0,
        paid: json['paid'] ?? 0.0);
  }

  factory Bill.dummy(id) {
    List<BillStatus> billStatus = [
      BillStatus.closed,
      BillStatus.editing,
      BillStatus.ready,
      BillStatus.sent
    ];
    Random rng = Random();
    int a = rng.nextInt(3);
    return Bill(
      id: id,
      dueDate: DateTime.now().add(Duration(days: id)),
      sendDate: DateTime.now().subtract(Duration(days: id)),
      asTva: id % 2 == 0,
      asPenaltyWarning: id == 1,
      tvaAmount: 12.5,
      payments: [
        Payment.dummy(int.parse('${id}1')),
        Payment.dummy(int.parse('${id}2')),
        Payment.dummy(int.parse('${id}3'))
      ],
      regulations: [
        Payment.dummy(int.parse('$id${id}1')),
        Payment.dummy(int.parse('$id${id}2'))
      ],
      status: billStatus[rng.nextInt(4)],
      createdAt: DateTime.now().subtract(Duration(days: id, hours: id)),
      total: id * 314.14,
      paid: a == 0
          ? id * 314.14
          : a == 1
              ? id * 212.1
              : id * 452.44,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['due_date'] = dueDate?.toIso8601String();
    data['send_date'] = sendDate?.toIso8601String();
    data['as_tva'] = asTva;
    data['as_penalty_warning'] = asPenaltyWarning;
    data['tva_amount'] = tvaAmount;
    data['payments'] = payments?.map((v) => v.toJson()).toList();
    data['regulations'] = regulations?.map((v) => v.toJson()).toList();
    data['status'] = billStatusToString(status);
    data['created_at'] = createdAt.toIso8601String();
    return data;
  }
}
