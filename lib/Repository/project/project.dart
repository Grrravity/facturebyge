import 'package:facture_by_ge/Repository/task/task.dart';
import 'package:hive/hive.dart';

part 'project.g.dart';

@HiveType(typeId: 3)
class Project extends HiveObject {
  @HiveField(0)
  int id;
  @HiveField(1)
  String? description;
  @HiveField(2)
  bool isOver;
  @HiveField(3)
  List<Task>? tasks;

  Project(
      {required this.id, this.description, required this.isOver, this.tasks});

  factory Project.fromJson(Map<String, dynamic> json) {
    return Project(
      id: json['id'] ?? -1,
      description: json['description'],
      isOver: json['isOver'] ?? false,
      tasks: List<Task>.from(json['tasks'].map((x) => Task.fromJson(x))),
    );
  }

  factory Project.dummy(int id) {
    return Project(
        id: id,
        description: "A dummy project $id",
        isOver: id % 2 == 0,
        tasks: [
          Task.dummy(int.parse('${id}1')),
          Task.dummy(int.parse('${id}2')),
          Task.dummy(int.parse('${id}3'))
        ]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['description'] = description;
    data['isOver'] = isOver;
    data['tasks'] = tasks?.map((v) => v.toJson()).toList();
    return data;
  }
}
