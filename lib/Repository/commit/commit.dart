import 'package:hive/hive.dart';

part 'commit.g.dart';

@HiveType(typeId: 5)
class Commit extends HiveObject {
  @HiveField(0)
  int id;
  @HiveField(1)
  String sha;
  @HiveField(2)
  String message;
  @HiveField(3)
  DateTime pushDate;
  @HiveField(4)
  String? branchName;
  @HiveField(5)
  String? commitLink;

  Commit(
      {required this.id,
      required this.sha,
      required this.message,
      required this.pushDate,
      this.branchName,
      this.commitLink});

  factory Commit.fromJson(Map<String, dynamic> json) {
    return Commit(
      id: json['id'] ?? -1,
      sha: json['sha'],
      message: json['message'] ?? "not provided",
      pushDate: DateTime.tryParse(json['push_date']) ?? DateTime(1881),
      branchName: json['branch_name'] ?? "not provided",
      commitLink: json['commit_link'] ?? "not provided",
    );
  }

  factory Commit.dummy(int id) {
    return Commit(
      id: id,
      sha: '${id}AZj${id}AZD5q5q7AQS',
      message: "Commit numéro ${id}",
      pushDate: DateTime.now().subtract(Duration(hours: id)),
      branchName: "origin/master",
      commitLink: "gitlab.com/grrravity/commit/$id",
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['sha'] = sha;
    data['message'] = message;
    data['push_date'] = pushDate.toIso8601String();
    data['branch_name'] = branchName;
    data['commit_link'] = commitLink;
    return data;
  }
}
