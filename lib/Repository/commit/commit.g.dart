// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'commit.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CommitAdapter extends TypeAdapter<Commit> {
  @override
  final int typeId = 5;

  @override
  Commit read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Commit(
      id: fields[0] as int,
      sha: fields[1] as String,
      message: fields[2] as String,
      pushDate: fields[3] as DateTime,
      branchName: fields[4] as String?,
      commitLink: fields[5] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, Commit obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.sha)
      ..writeByte(2)
      ..write(obj.message)
      ..writeByte(3)
      ..write(obj.pushDate)
      ..writeByte(4)
      ..write(obj.branchName)
      ..writeByte(5)
      ..write(obj.commitLink);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CommitAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
