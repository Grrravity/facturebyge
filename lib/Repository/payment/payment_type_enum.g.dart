// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_type_enum.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PaymentTypeAdapter extends TypeAdapter<PaymentType> {
  @override
  final int typeId = 9;

  @override
  PaymentType read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return PaymentType.creditCart;
      case 1:
        return PaymentType.check;
      case 2:
        return PaymentType.bankTransfert;
      case 3:
        return PaymentType.cash;
      case 4:
        return PaymentType.none;
      default:
        return PaymentType.creditCart;
    }
  }

  @override
  void write(BinaryWriter writer, PaymentType obj) {
    switch (obj) {
      case PaymentType.creditCart:
        writer.writeByte(0);
        break;
      case PaymentType.check:
        writer.writeByte(1);
        break;
      case PaymentType.bankTransfert:
        writer.writeByte(2);
        break;
      case PaymentType.cash:
        writer.writeByte(3);
        break;
      case PaymentType.none:
        writer.writeByte(4);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PaymentTypeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
