import 'package:hive/hive.dart';

part 'payment_type_enum.g.dart';

@HiveType(typeId: 9)
enum PaymentType {
  @HiveField(0)
  creditCart,
  @HiveField(1)
  check,
  @HiveField(2)
  bankTransfert,
  @HiveField(3)
  cash,
  @HiveField(4)
  none,
}

String paymentTypeToString(PaymentType value) {
  switch (value) {
    case PaymentType.creditCart:
      return 'creditCart';
    case PaymentType.check:
      return 'check';
    case PaymentType.bankTransfert:
      return 'bankTransfert';
    case PaymentType.cash:
      return 'cash';
    case PaymentType.none:
      return 'none';
    default:
      return 'none';
  }
}

PaymentType stringToPaymentType(String value) {
  switch (value) {
    case 'creditCart':
      return PaymentType.creditCart;
    case 'check':
      return PaymentType.check;
    case 'bankTransfert':
      return PaymentType.bankTransfert;
    case 'cash':
      return PaymentType.cash;
    case 'none':
      return PaymentType.none;
    default:
      return PaymentType.none;
  }
}
