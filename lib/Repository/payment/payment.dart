import 'dart:math';

import 'package:facture_by_ge/Repository/payment/payment_type_enum.dart';
import 'package:hive/hive.dart';

part 'payment.g.dart';

@HiveType(typeId: 8)
class Payment extends HiveObject {
  @HiveField(0)
  int id;
  @HiveField(1)
  double amount;
  @HiveField(2)
  PaymentType type;
  @HiveField(3)
  String? bankIdentifier;
  @HiveField(4)
  DateTime date;
  @HiveField(5)
  bool isDeptRecovery;

  Payment(
      {required this.id,
      required this.amount,
      required this.type,
      this.bankIdentifier,
      required this.date,
      required this.isDeptRecovery});

  factory Payment.fromJson(Map<String, dynamic> json) {
    return Payment(
      id: json['id'] ?? -1,
      amount: json['amount'] ?? 0.0,
      type: stringToPaymentType(json['type']),
      bankIdentifier: json['bank_identifier'],
      date: json['date'] ?? "not provided",
      isDeptRecovery: json['is_dept_recovery'] ?? false,
    );
  }

  factory Payment.dummy(int id) {
    List<PaymentType> paymentType = [
      PaymentType.creditCart,
      PaymentType.check,
      PaymentType.bankTransfert,
      PaymentType.none,
    ];
    Random rng = Random();
    return Payment(
        id: id,
        amount: 4514.41,
        type: paymentType[rng.nextInt(3)],
        bankIdentifier: "IP 11${id}24 AGRI",
        date: DateTime.now().subtract(const Duration(days: 3)),
        isDeptRecovery: false);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['amount'] = amount;
    data['type'] = paymentTypeToString(type);
    data['bank_identifier'] = bankIdentifier;
    data['date'] = date.toIso8601String();
    data['is_dept_recovery'] = isDeptRecovery;
    return data;
  }
}
