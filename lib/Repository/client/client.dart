import 'dart:math';

import 'package:facture_by_ge/Repository/bill/bill.dart';
import 'package:facture_by_ge/Repository/project/project.dart';
import 'package:hive/hive.dart';

part 'client.g.dart';

@HiveType(typeId: 1)
class Client extends HiveObject {
  @HiveField(0)
  int id;
  @HiveField(1)
  String companyName;
  @HiveField(2)
  String? address;
  @HiveField(3)
  String? city;
  @HiveField(4)
  String? zip;
  @HiveField(5)
  String? country;
  @HiveField(6)
  String? firstName;
  @HiveField(7)
  String? lastName;
  @HiveField(8)
  String? siret;
  @HiveField(9)
  List<Bill>? bills;
  @HiveField(10)
  List<Project>? projects;
  @HiveField(11)
  DateTime createdAt;

  Client(
      {required this.id,
      required this.companyName,
      this.address,
      this.city,
      this.zip,
      this.country,
      this.firstName,
      this.lastName,
      this.siret,
      this.bills,
      this.projects,
      required this.createdAt});

  factory Client.fromJson(Map<String, dynamic> json) {
    return Client(
      id: json['id'] ?? -1,
      companyName: json['company_name'] ?? 'unspecified',
      address: json['address'],
      city: json['city'],
      zip: json['zip'],
      country: json['country'],
      firstName: json['first_name'],
      lastName: json['last_name'],
      siret: json['siret'],
      bills: List<Bill>.from(json['bills'].map((x) => Bill.fromJson(x))),
      projects:
          List<Project>.from(json['projects'].map((x) => Project.fromJson(x))),
      createdAt: DateTime.tryParse(json['created_at']) ?? DateTime.now(),
    );
  }

  factory Client.dummy(int id) {
    List<String> _name = ['Jean', 'Mark', 'Patrice'];
    List<String> _lastname = ['Popof', 'Gaston', 'Merigald'];
    Random rng = Random();
    return Client(
        id: id,
        companyName: '${id}cv1job',
        address: '${id}57 rue camille',
        city: 'Lyon',
        zip: '6900$id',
        country: 'France',
        firstName: _name[rng.nextInt(2)],
        lastName: _lastname[rng.nextInt(2)],
        siret: '000' + (11122233333 * id).toString(),
        bills: [
          Bill.dummy(int.parse('${id}1')),
          Bill.dummy(int.parse('${id}2'))
        ],
        projects: [
          Project.dummy(int.parse('${id}1')),
          Project.dummy(int.parse('${id}2'))
        ],
        createdAt: DateTime.now().subtract(Duration(days: id)));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['company_name'] = companyName;
    data['address'] = address;
    data['city'] = city;
    data['zip'] = zip;
    data['country'] = country;
    data['first_name'] = firstName;
    data['last_name'] = lastName;
    data['siret'] = siret;
    data['bills'] = bills?.map((v) => v.toJson()).toList();
    data['projects'] = bills?.map((v) => v.toJson()).toList();
    data['created_at'] == createdAt.toIso8601String();
    return data;
  }
}
