// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'client.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ClientAdapter extends TypeAdapter<Client> {
  @override
  final int typeId = 1;

  @override
  Client read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Client(
      id: fields[0] as int,
      companyName: fields[1] as String,
      address: fields[2] as String?,
      city: fields[3] as String?,
      zip: fields[4] as String?,
      country: fields[5] as String?,
      firstName: fields[6] as String?,
      lastName: fields[7] as String?,
      siret: fields[8] as String?,
      bills: (fields[9] as List?)?.cast<Bill>(),
      projects: (fields[10] as List?)?.cast<Project>(),
      createdAt: fields[11] as DateTime,
    );
  }

  @override
  void write(BinaryWriter writer, Client obj) {
    writer
      ..writeByte(12)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.companyName)
      ..writeByte(2)
      ..write(obj.address)
      ..writeByte(3)
      ..write(obj.city)
      ..writeByte(4)
      ..write(obj.zip)
      ..writeByte(5)
      ..write(obj.country)
      ..writeByte(6)
      ..write(obj.firstName)
      ..writeByte(7)
      ..write(obj.lastName)
      ..writeByte(8)
      ..write(obj.siret)
      ..writeByte(9)
      ..write(obj.bills)
      ..writeByte(10)
      ..write(obj.projects)
      ..writeByte(11)
      ..write(obj.createdAt);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ClientAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
