import 'package:facture_by_ge/Repository/commit/commit.dart';
import 'package:facture_by_ge/Repository/pricing/pricing.dart';
import 'package:facture_by_ge/Repository/project/project.dart';
import 'package:hive/hive.dart';

part 'task.g.dart';

@HiveType(typeId: 4)
class Task extends HiveObject {
  @HiveField(0)
  int id;
  @HiveField(1)
  String? description;
  @HiveField(2)
  List<Commit>? commits;
  @HiveField(3)
  double? length;
  @HiveField(4)
  Pricing pricing;
  @HiveField(5)
  bool? isDiscounted;
  @HiveField(6)
  double? discountAmount;

  Task(
      {required this.id,
      this.description,
      this.commits,
      this.length,
      required this.pricing,
      this.isDiscounted,
      this.discountAmount});

  factory Task.fromJson(Map<String, dynamic> json) {
    return Task(
      id: json['id'] ?? -1,
      description: json['description'] ?? "unknown",
      commits: List<Commit>.from(json['commit'].map((x) => Commit.fromJson(x))),
      length: json['length'] ?? 15,
      pricing: Pricing.fromJson(json['pricing']),
      isDiscounted: json['is_discounted'] ?? false,
      discountAmount: json['discountAmount'] ?? 0.0,
    );
  }

  factory Task.dummy(int id) {
    return Task(
        id: id,
        description: 'A task description $id',
        commits: [
          Commit.dummy(int.parse('${id}1')),
          Commit.dummy(int.parse('${id}2'))
        ],
        pricing: Pricing.dummy(id),
        length: id * 4,
        isDiscounted: id % 2 == 0,
        discountAmount: (id % 2 == 0) ? id.toDouble() : 0.0);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['description'] = description;
    data['commit'] = commits?.map((v) => v.toJson()).toList();
    data['length'] = length;
    data['pricing'] = pricing.toJson();
    data['is_discounted'] = isDiscounted;
    data['discountAmount'] = discountAmount;
    return data;
  }
}
