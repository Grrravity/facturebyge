// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TaskAdapter extends TypeAdapter<Task> {
  @override
  final int typeId = 4;

  @override
  Task read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Task(
      id: fields[0] as int,
      description: fields[1] as String?,
      commits: (fields[2] as List?)?.cast<Commit>(),
      length: fields[3] as double?,
      pricing: fields[4] as Pricing,
      isDiscounted: fields[5] as bool?,
      discountAmount: fields[6] as double?,
    );
  }

  @override
  void write(BinaryWriter writer, Task obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.description)
      ..writeByte(2)
      ..write(obj.commits)
      ..writeByte(3)
      ..write(obj.length)
      ..writeByte(4)
      ..write(obj.pricing)
      ..writeByte(5)
      ..write(obj.isDiscounted)
      ..writeByte(6)
      ..write(obj.discountAmount);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TaskAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
