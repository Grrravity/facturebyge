import 'package:facture_by_ge/Core/hive_box.dart';
import 'package:facture_by_ge/Repository/bill/bill.dart';
import 'package:facture_by_ge/Repository/client/client.dart';
import 'package:facture_by_ge/Repository/freelance/freelance.dart';
import 'package:facture_by_ge/Repository/payment/payment.dart';
import 'package:facture_by_ge/Repository/task/task.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';

class BillingViewController extends GetxController {
  Rx<Freelance> freelance = Freelance.dummy().obs;
  RxList<Bill> bills = <Bill>[].obs;
  RxList<Client> clients = <Client>[].obs;
  RxList<Task> tasks = <Task>[].obs;
  RxList<Payment> payments = <Payment>[].obs;
  late Box<Bill> billBox;
  late Box<Freelance> freelanceBox;
  late Box<Client> clientBox;
  late Box<Task> taskBox;
  late Box<Payment> paymentBox;

  RxInt selectedBillIndex = (-1).obs;
  @override
  void onInit() async {
    billBox = Hive.box<Bill>(BoxKey.bill);
    freelanceBox = Hive.box<Freelance>(BoxKey.freelance);
    clientBox = Hive.box<Client>(BoxKey.client);
    taskBox = Hive.box<Task>(BoxKey.task);
    paymentBox = Hive.box<Payment>(BoxKey.payment);
    super.onInit();
  }

  @override
  void onReady() {
    freelance.value = freelanceBox.getAt(0)!;
    bills.value = billBox.values.toList();
    clients.value = clientBox.values.toList();
    tasks.value = taskBox.values.toList();
    payments.value = paymentBox.values.toList();
    bills.value.sort((a, b) {
      if (a.dueDate != null) {
        if (b.dueDate != null) {
          return a.dueDate!.compareTo(b.dueDate!);
        } else {
          return 1;
        }
      } else {
        return 1;
      }
    });
    super.onReady();
  }

  String getAmont() {
    //Map<int, double> toPayList = {};

    //for (var task in tasks.entries) {
    //  if (bills.entries
    //      .any((element) => element.value.taskIds.contains(task.value.id))) {
    //    toPayList.addAll({task.value.id: task.value.pricing.amount});
    //  }
    //}

    //Map<int, double> paidList = {};
    //for (var payment in payments.entries) {
    //if (bills.entries
    //    .any((element) => element.value.paymentId == payment.value.id)) {
    //      paidList.addAll({:payment.value.amount})
    //    }
    //}

    //double amount = 0;
    //List<int> taskIds = [];
    //for (var task in tasks.entries) {
    //  if (taskIds.contains(task.value.id)) {
    //    amount = amount + task.value.pricing.amount;
    //  }
    //}
    //return amount.toStringAsFixed(2);
    return 'oi';
  }

  updateUi() {
    update();
  }
}
