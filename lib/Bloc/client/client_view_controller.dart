import 'package:facture_by_ge/Core/hive_box.dart';
import 'package:facture_by_ge/Core/ui_constants.dart';
import 'package:facture_by_ge/Repository/bill/bill.dart';
import 'package:facture_by_ge/Repository/client/client.dart';
import 'package:facture_by_ge/Repository/freelance/freelance.dart';
import 'package:facture_by_ge/Repository/payment/payment.dart';
import 'package:facture_by_ge/Repository/task/task.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

class ClientViewController extends GetxController {
  Rx<Freelance> freelance = Freelance.dummy().obs;
  RxList<Bill> bills = <Bill>[].obs;
  RxList<Client> clients = <Client>[].obs;
  RxList<Task> tasks = <Task>[].obs;
  RxList<Payment> payments = <Payment>[].obs;
  late Box<Bill> billBox;
  late Box<Freelance> freelanceBox;
  late Box<Client> clientBox;
  late Box<Task> taskBox;
  late Box<Payment> paymentBox;

  @override
  void onInit() async {
    billBox = Hive.box<Bill>(BoxKey.bill);
    freelanceBox = Hive.box<Freelance>(BoxKey.freelance);
    clientBox = Hive.box<Client>(BoxKey.client);
    taskBox = Hive.box<Task>(BoxKey.task);
    paymentBox = Hive.box<Payment>(BoxKey.payment);
    super.onInit();
  }

  @override
  void onReady() {
    freelance.value = freelanceBox.getAt(0)!;
    bills.value = billBox.values.toList();
    clients.value = clientBox.values.toList();
    tasks.value = taskBox.values.toList();
    payments.value = paymentBox.values.toList();
    super.onReady();
  }

  List<dynamic> getDatas(int index) {
    switch (index) {
      case 0:
        return [
          'Nombre de clients',
          'Depuis toujours',
          _getClientBase(),
          [LineAwesomeIcons.user_tie, UiConstants.secondaryBlue],
          [100.0, UiConstants.secondaryBlue],
        ];
      case 1:
        return [
          'Nouveaux clients',
          'créés il y a moins de 4 jours',
          _getNewClient(),
          [LineAwesomeIcons.user_plus, UiConstants.secondaryGreen],
          _getNewClientEvaluation(),
        ];
      case 2:
        return [
          'Clients reconvertis',
          'Ayant repassé commande ces 14 derniers jours',
          getReturningClient(),
          [LineAwesomeIcons.alternate_sync, UiConstants.secondaryGreen],
          _getReturningClientEvaluation(),
        ];
      case 3:
        return [
          'Total payé',
          "Sur l'ensemble des factures",
          _getIncomes(),
          [LineAwesomeIcons.coins, UiConstants.secondaryBlue],
          [100.0, UiConstants.secondaryBlue]
        ];
      case 4:
        return [
          'A percevoir',
          "Sur l'ensemble des factures",
          _getAwaitingBillsValue(),
          [LineAwesomeIcons.hand_holding_us_dollar, UiConstants.secondaryGreen],
          _getAwaitingBillsEvaluation(),
        ];
      case 5:
        return [
          'A rembourser',
          "Sur l'ensemble des factures",
          _getAmountToRefund(),
          [
            LineAwesomeIcons.balance_scale__right_weighted_,
            UiConstants.secondaryRed
          ],
          _getAmountToRefundEvaluation(),
        ];
      default:
        return [
          'Title',
          'Subtitle',
          ['Data', false],
          [UiConstants.secondaryGreen, LineAwesomeIcons.question],
          [100.0, UiConstants.secondaryBlue],
        ];
    }
  }

  List<dynamic> _getClientBase() {
    return [clients.length, false];
  }

  List<dynamic> _getNewClient() {
    //TODO changer le nombre de jour
    DateTime now = DateTime.now().subtract(const Duration(days: 4));
    List<Client> _clients = clients.value;
    _clients.removeWhere((e) => now.isAfter(e.createdAt));
    return [_clients.length, false];
  }

  List<dynamic> _getNewClientEvaluation() {
    int value = _getNewClient()[0] - _getNewClient()[0];
    //TODO change le goal
    int objective = 14;
    double data = value / objective > 1 ? 1 : value / objective;
    if (data < 0.25) {
      return [data < 0.05 ? 0.05 : data, UiConstants.secondaryRed];
    } else if (data < 0.50) {
      return [data, UiConstants.secondaryYellow];
    } else if (data < 0.75) {
      return [data, UiConstants.secondaryBlue];
    } else {
      return [data, UiConstants.secondaryGreen];
    }
  }

  List<dynamic> getReturningClient() {
    DateTime now = DateTime.now().subtract(const Duration(days: 15));
    List<Client> _clients = clients.value;
    _clients.removeWhere((e) => (e.bills?.length ?? 0) <= 1);
    _clients
        .removeWhere((e) => e.bills!.any((e1) => now.isBefore(e1.createdAt)));
    return [_clients.length, false];
  }

  List<dynamic> _getReturningClientEvaluation() {
    int value = _getNewClient()[0] - getReturningClient()[0];
    //TODO change le goal
    int objective = 7;
    double data = value / objective > 1 ? 1 : value / objective;
    if (data < 0.25) {
      return [data < 0.05 ? 0.05 : data, UiConstants.secondaryRed];
    } else if (data < 0.50) {
      return [data, UiConstants.secondaryYellow];
    } else if (data < 0.75) {
      return [data, UiConstants.secondaryBlue];
    } else {
      return [data, UiConstants.secondaryGreen];
    }
  }

  List<dynamic> _getIncomes() {
    double incomes = 0;
    for (var bill in bills.value) {
      incomes = incomes + (bill.paid ?? 0);
    }
    return [incomes.toPrecision(2), true];
  }

  List<dynamic> _getAwaitingBillsValue() {
    List<Bill> _bills = [];
    _bills.addAll(bills.value);
    _bills.removeWhere((e) => e.total <= (e.paid ?? 0));
    double sum = 0;
    _bills.forEach((e) {
      sum = sum + (e.total - (e.paid ?? 0));
    });
    return [sum.toPrecision(2), true];
  }

  List<dynamic> _getAwaitingBillsEvaluation() {
    double value = _getAwaitingBillsValue()[0];
    //TODO change le goal
    double objective = 1800.00;
    double data = value / objective > 1 ? 1 : value / objective;
    if (data < 0.25) {
      return [data < 0.05 ? 0.05 : data, UiConstants.secondaryRed];
    } else if (data < 0.50) {
      return [data, UiConstants.secondaryYellow];
    } else if (data < 0.75) {
      return [data, UiConstants.secondaryBlue];
    } else {
      return [data, UiConstants.secondaryGreen];
    }
  }

  List<dynamic> _getAmountToRefund() {
    List<Bill> _bills = [];
    _bills.addAll(bills.value);
    _bills.removeWhere((e) => e.total >= (e.paid ?? 0));
    double sum = 0;
    _bills.forEach((e) {
      sum = sum + ((e.paid ?? 0) - e.total);
    });
    return [sum.toPrecision(2), true];
  }

  List<dynamic> _getAmountToRefundEvaluation() {
    double value = _getAwaitingBillsValue()[0];
    //TODO change le goal
    double objective = 1800.00;
    double data = value / objective > 1 ? 1 : value / objective;
    if (data < 0.25) {
      return [data < 0.05 ? 0.05 : data, UiConstants.secondaryGreen];
    } else if (data < 0.50) {
      return [data, UiConstants.secondaryBlue];
    } else if (data < 0.75) {
      return [data, UiConstants.secondaryYellow];
    } else {
      return [data, UiConstants.secondaryRed];
    }
  }
}
