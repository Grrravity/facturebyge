import 'package:facture_by_ge/Bloc/billing/billing_view_controller.dart';
import 'package:facture_by_ge/Core/ui_constants.dart';
import 'package:facture_by_ge/Repository/bill/bill_status_enum.dart';
import 'package:facture_by_ge/Screen/Layout/skeleton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:pdf/widgets.dart' as pw;

import 'Layout/widget/pdf_generator.dart';

class BillingView extends StatelessWidget {
  final BillingViewController controller = Get.put(BillingViewController());

  BillingView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final pdf = pw.Document();
    double deviceWidth = MediaQuery.of(context).size.width,
        deviceHeight = MediaQuery.of(context).size.height;
    ScrollController scrollController = ScrollController();
    return MainSkeleton(
      currentIndex: 0,
      body: Row(
        children: [
          Container(
            color: Colors.white,
            height: deviceHeight,
            width: deviceWidth * 0.25 - (UiConstants.leftMenuSize / 2),
            padding: const EdgeInsets.fromLTRB(18, 24, 18, 0),
            child: Column(
              children: [
                const Align(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                      height: 45,
                      child: Text(
                        'Liste des factures',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 24),
                      )),
                ),
                Center(
                  child: Container(
                      height: 1,
                      width:
                          deviceWidth * 0.25 - (UiConstants.leftMenuSize / 2),
                      color: Colors.grey[300]),
                ),
                const SizedBox(
                  height: 28,
                ),
                Obx(
                  () => SizedBox(
                    height: MediaQuery.of(context).size.height - 98,
                    child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      controller: scrollController,
                      itemCount: controller.bills.length,
                      itemBuilder: _getItemList,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            color: UiConstants.backgroundLight,
            height: deviceHeight,
            width: deviceWidth * 0.75 - (UiConstants.leftMenuSize / 2),
            child: const PdfFacture(),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => print('create facture'),
        tooltip: 'Créer une facture',
        child: const Icon(
          LineAwesomeIcons.plus_circle,
          color: Colors.blueAccent,
        ),
        backgroundColor: Colors.white,
      ),
    );
  }

  Widget _getItemList(
    BuildContext context,
    int index,
  ) {
    double deviceWidth = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        controller.selectedBillIndex.value = index;
        controller.updateUi();
      },
      child: Obx(
        () => Container(
          margin: const EdgeInsets.only(bottom: 18),
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 18),
          height: 87,
          decoration: BoxDecoration(
            color: UiConstants.backgroundLight.withOpacity(0.4),
            border: Border.all(
                color: UiConstants.primaryBlue,
                width: 1,
                style: controller.selectedBillIndex.value == index
                    ? BorderStyle.solid
                    : BorderStyle.none),
            borderRadius: const BorderRadius.all(Radius.circular(12)),
          ),
          child: Container(
            height: 87,
            width: deviceWidth * 0.25 -
                (UiConstants.leftMenuSize / 2) -
                24 -
                87 -
                120,
            margin: const EdgeInsets.only(right: 12),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: 38,
                  height: 38,
                  margin: const EdgeInsets.only(right: 12),
                  decoration: const BoxDecoration(
                    color: Colors.blueAccent,
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ),
                  child: Center(
                      child: Text(
                    controller.clients
                        .firstWhere((client) =>
                            client.bills?.any((bill) =>
                                bill.id == controller.bills[index].id) ??
                            false)
                        .companyName
                        .substring(0, 2),
                    style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  )),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20,
                        child: Text("Facture n° " +
                            controller.bills[index].id.toString()),
                      ),
                      if (controller.bills[index].status ==
                              BillStatus.editing ||
                          controller.bills[index].status == BillStatus.ready)
                        Text(
                            "Créé le ${DateFormat.yMMMMd('fr_FR').format(controller.bills[index].createdAt)}"),
                      if (controller.bills[index].status !=
                              BillStatus.editing &&
                          controller.bills[index].dueDate != null)
                        Text(
                            "Due le ${DateFormat.yMMMMd('fr_FR').format(controller.bills[index].dueDate!)}"),
                      if (controller.bills[index].status == BillStatus.sent &&
                          controller.bills[index].sendDate != null)
                        Text(
                            "Envoyée le ${DateFormat.yMMMMd('fr_FR').format(controller.bills[index].sendDate!)}"),
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      height: 36,
                      width: 84,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color:
                            billStatusToColor(controller.bills[index].status),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8)),
                      ),
                      child: Text(
                        billStatusToString(controller.bills[index].status),
                        style: TextStyle(
                            color: billStatusToTextColor(
                                controller.bills[index].status)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
