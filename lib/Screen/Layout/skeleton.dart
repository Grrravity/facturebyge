import 'package:facture_by_ge/Core/routes.dart';
import 'package:facture_by_ge/Core/ui_constants.dart';
import 'package:facture_by_ge/Screen/Layout/widget/custom_bot_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

class MainSkeleton extends StatefulWidget {
  final Widget body;
  final int currentIndex;
  final Widget? floatingActionButton;
  const MainSkeleton({
    Key? key,
    required this.body,
    required this.currentIndex,
    this.floatingActionButton,
  }) : super(key: key);

  @override
  State<MainSkeleton> createState() => _MainSkeletonState();
}

class _MainSkeletonState extends State<MainSkeleton> {
  @override
  Widget build(BuildContext context) {
    int _selectedIndex = widget.currentIndex;

    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            Positioned(
              top: 0,
              left: UiConstants.leftMenuSize,
              child: SizedBox(
                width: MediaQuery.of(context).size.width - 80,
                height: MediaQuery.of(context).size.height,
                child: Center(
                  key: const Key('body'),
                  child: widget.body,
                ),
              ),
            ),
            Positioned(
              top: 0,
              left: 0,
              child: Container(
                width: UiConstants.leftMenuSize,
                decoration:
                    const BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 6,
                    offset: Offset(4, 0),
                  ),
                ]),
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Tooltip(
                      message: "Factures by G-E",
                      child: Container(
                        height: 50,
                        width: 50,
                        margin: const EdgeInsets.symmetric(vertical: 24),
                        decoration: BoxDecoration(
                          color: Colors.blueAccent[700],
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: const Center(
                          child: Text(
                            'F',
                            style: TextStyle(
                                fontSize: 28,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    Tooltip(
                      message: "Mes factures",
                      child: Container(
                        height: 50,
                        width: 50,
                        margin: const EdgeInsets.only(bottom: 18, top: 12),
                        decoration: BoxDecoration(
                          color: _selectedIndex == 0
                              ? Colors.blueAccent[700]!.withOpacity(0.1)
                              : Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Center(
                          child: IconButton(
                              onPressed: () {
                                setState(() {
                                  _selectedIndex = 0;
                                  onItemTapped(0);
                                });
                              },
                              icon: Icon(LineAwesomeIcons.receipt,
                                  size: 24,
                                  color: _selectedIndex == 0
                                      ? Colors.blueAccent[700]
                                      : Colors.grey)),
                        ),
                      ),
                    ),
                    Tooltip(
                      message: "Mes clients",
                      child: Container(
                        height: 50,
                        width: 50,
                        margin: const EdgeInsets.only(bottom: 18),
                        decoration: BoxDecoration(
                          color: _selectedIndex == 1
                              ? Colors.blueAccent[700]!.withOpacity(0.1)
                              : Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Center(
                          child: IconButton(
                              onPressed: () {
                                setState(() {
                                  _selectedIndex = 1;
                                  onItemTapped(1);
                                });
                              },
                              icon: Icon(LineAwesomeIcons.briefcase,
                                  size: 24,
                                  color: _selectedIndex == 1
                                      ? Colors.blueAccent[700]
                                      : Colors.grey)),
                        ),
                      ),
                    ),
                    Tooltip(
                      message: "Mes projets",
                      child: Container(
                        height: 50,
                        width: 50,
                        margin: const EdgeInsets.only(bottom: 18),
                        decoration: BoxDecoration(
                          color: _selectedIndex == 2
                              ? Colors.blueAccent[700]!.withOpacity(0.1)
                              : Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Center(
                          child: IconButton(
                              onPressed: () {
                                setState(() {
                                  _selectedIndex = 2;
                                  onItemTapped(2);
                                });
                              },
                              icon: Icon(LineAwesomeIcons.lightbulb,
                                  size: 24,
                                  color: _selectedIndex == 2
                                      ? Colors.blueAccent[700]
                                      : Colors.grey)),
                        ),
                      ),
                    ),
                    Tooltip(
                      message: "intégration Git",
                      child: Container(
                        height: 50,
                        width: 50,
                        margin: const EdgeInsets.only(bottom: 18),
                        decoration: BoxDecoration(
                          color: _selectedIndex == 3
                              ? Colors.blueAccent[700]!.withOpacity(0.1)
                              : Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Center(
                          child: IconButton(
                              onPressed: () {
                                setState(() {
                                  _selectedIndex = 3;
                                  onItemTapped(3);
                                });
                              },
                              icon: Icon(LineAwesomeIcons.git,
                                  size: 24,
                                  color: _selectedIndex == 3
                                      ? Colors.blueAccent[700]
                                      : Colors.grey)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        //bottomNavigationBar: _buildCustomBotBar(_selectedIndex),
        //BottomNavigationBar(
        //  items: ,
        //  currentIndex: _selectedIndex,
        //  unselectedItemColor: Colors.white,
        //  unselectedLabelStyle: TextStyle(color: Colors.white),
        //  showSelectedLabels: true,
        //  onTap: _onItemTapped,
        //),
        floatingActionButton: Padding(
            padding: EdgeInsets.only(bottom: 45),
            child: widget.floatingActionButton),
      ),
    );
  }

  Widget _buildCustomBotBar(int _selectedIndex) {
    return CustomBottomBar(
      containerHeight: 70,
      backgroundColor: Colors.black,
      selectedIndex: _selectedIndex,
      showElevation: true,
      itemCornerRadius: 24,
      onItemSelected: (index) => setState(() {
        _selectedIndex = index;
        onItemTapped(index);
      }),
      items: <BottomNavyBarItem>[
        BottomNavyBarItem(
          icon: const Icon(LineAwesomeIcons.receipt),
          title: const Text('Factures'),
          activeColor: Colors.greenAccent,
          inactiveColor: Colors.white,
          textAlign: TextAlign.center,
        ),
        BottomNavyBarItem(
          icon: const Icon(LineAwesomeIcons.briefcase),
          title: const Text('Clients'),
          activeColor: Colors.purpleAccent,
          inactiveColor: Colors.white,
          textAlign: TextAlign.center,
        ),
        BottomNavyBarItem(
          icon: const Icon(LineAwesomeIcons.lightbulb),
          title: const Text(
            'Projets',
          ),
          activeColor: Colors.redAccent,
          inactiveColor: Colors.white,
          textAlign: TextAlign.center,
        ),
        BottomNavyBarItem(
          icon: const Icon(LineAwesomeIcons.git),
          title: const Text('Repo'),
          activeColor: Colors.yellowAccent,
          inactiveColor: Colors.white,
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

  onItemTapped(int index) {
    switch (index) {
      case 0:
        Get.offAndToNamed(Routes.billing);
        break;
      case 1:
        Get.offAndToNamed(Routes.client);
        break;
      case 2:
        Get.offAndToNamed(Routes.project);
        break;
      case 3:
        Get.offAndToNamed(Routes.repo);
        break;
      default:
        Get.offAndToNamed(Routes.billing);
        break;
    }
  }
}
