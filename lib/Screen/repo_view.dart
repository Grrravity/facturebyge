import 'package:facture_by_ge/Bloc/repo/repo_view_controller.dart';
import 'package:facture_by_ge/Screen/Layout/skeleton.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

class RepoView extends StatelessWidget {
  final RepoViewController controller = Get.put(RepoViewController());

  RepoView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MainSkeleton(
      currentIndex: 3,
      body: TextButton(onPressed: () => null, child: const Text('Repo')),
      floatingActionButton: FloatingActionButton(
        onPressed: () => print('add repo'),
        tooltip: 'Ajouter un repository',
        child: const Icon(
          LineAwesomeIcons.plus_circle,
          color: Colors.yellowAccent,
        ),
        backgroundColor: Colors.black,
      ),
    );
  }
}
