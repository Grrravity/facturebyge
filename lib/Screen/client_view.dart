import 'dart:math' show pi;

import 'package:facture_by_ge/Bloc/client/client_view_controller.dart';
import 'package:facture_by_ge/Core/ui_constants.dart';
import 'package:facture_by_ge/Screen/Layout/skeleton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

class ClientView extends StatelessWidget {
  final ClientViewController controller = Get.put(ClientViewController());

  ClientView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    ScrollController gridViewController = ScrollController();
    double deviceWidth = MediaQuery.of(context).size.width,
        deviceHeight = MediaQuery.of(context).size.height;
    return MainSkeleton(
      currentIndex: 1,
      body: Row(
        children: [
          Container(
            color: Colors.white,
            height: deviceHeight,
            width: deviceWidth * 0.25 - (UiConstants.leftMenuSize / 2),
            padding: const EdgeInsets.fromLTRB(18, 24, 18, 0),
            child: Column(
              children: [
                const Align(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                      height: 45,
                      child: Text(
                        'Mes clients',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 24),
                      )),
                ),
                Center(
                  child: Container(
                      height: 1,
                      width: deviceWidth * 0.25 -
                          (UiConstants.leftMenuSize / 2) -
                          36,
                      color: Colors.grey[300]),
                ),
                const SizedBox(
                  height: 28,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height - 98,
                  width:
                      deviceWidth * 0.25 - (UiConstants.leftMenuSize / 2) - 36,
                  child: ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    itemCount: 6,
                    itemBuilder: _getStatList,
                  ),
                ),
              ],
            ),
          ),
          Container(
            color: UiConstants.backgroundLight,
            height: deviceHeight,
            width: deviceWidth * 0.75 - (UiConstants.leftMenuSize / 2),
            child: Container(
              padding: const EdgeInsets.fromLTRB(28, 20, 28, 20),
              child: Column(
                children: [
                  Container(
                    height: 36,
                    width: (deviceWidth * 0.75 -
                        (UiConstants.leftMenuSize / 2) -
                        56),
                    color: UiConstants.backgroundWhite,
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  Container(
                    width: (deviceWidth * 0.75 -
                        (UiConstants.leftMenuSize / 2) -
                        56),
                    height: deviceHeight - 112,
                    decoration: const BoxDecoration(
                      color: UiConstants.backgroundWhite,
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.symmetric(
                              horizontal: 23, vertical: 32),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                children: [
                                  Container(
                                      height: 40,
                                      width: 90,
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 17, vertical: 12),
                                      decoration: const BoxDecoration(
                                        color: UiConstants.primaryBlue10,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8)),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: const [
                                          Icon(LineAwesomeIcons.list_ol,
                                              size: 16,
                                              color: UiConstants.primaryBlue),
                                          Text('LISTE',
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  color:
                                                      UiConstants.primaryBlue,
                                                  fontWeight: FontWeight.bold))
                                        ],
                                      )),
                                  const SizedBox(width: 4),
                                  Container(
                                      height: 40,
                                      width: 90,
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 15, vertical: 12),
                                      decoration: const BoxDecoration(
                                        color: UiConstants.primaryBlue,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8)),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: const [
                                          Icon(LineAwesomeIcons.border_all,
                                              size: 16,
                                              color:
                                                  UiConstants.backgroundWhite),
                                          Text('GRILLE',
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  color: UiConstants
                                                      .backgroundWhite,
                                                  fontWeight: FontWeight.bold))
                                        ],
                                      ))
                                ],
                              ),
                              Container(
                                width: 121,
                                height: 18,
                                alignment: Alignment.centerRight,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    const Icon(
                                      LineAwesomeIcons.filter,
                                      color: UiConstants.primaryGrey,
                                      size: 18,
                                    ),
                                    const Text(
                                      "FILTRER :",
                                      style: TextStyle(
                                          color: UiConstants.primaryGrey,
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Container(
                                      width: 37,
                                      height: 18,
                                      alignment: Alignment.center,
                                      child: const Text(
                                        "A-Z",
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Transform.rotate(
                                        angle: pi,
                                        child: const Icon(
                                          LineAwesomeIcons.angle_up,
                                          color: UiConstants.primaryGrey,
                                          size: 16,
                                        ))
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          color: UiConstants.backgroundLight.withOpacity(0.4),
                          height: 48,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 26, vertical: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                  width: 224,
                                  height: 16,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: const [
                                      Text('Projet',
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: UiConstants.primaryGrey,
                                          )),
                                      Icon(
                                        LineAwesomeIcons.sort_alphabetical_down,
                                        size: 16,
                                        color: UiConstants.primaryGrey,
                                      )
                                    ],
                                  )),
                              SizedBox(
                                  width: 224,
                                  height: 16,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: const [
                                      Text('Client',
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: UiConstants.primaryGrey,
                                          )),
                                      Icon(
                                        LineAwesomeIcons.sort_alphabetical_down,
                                        size: 16,
                                        color: UiConstants.primaryGrey,
                                      )
                                    ],
                                  )),
                              SizedBox(
                                  width: 224,
                                  height: 16,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: const [
                                      Text('Paiement à venir',
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: UiConstants.primaryGrey,
                                          )),
                                      Icon(
                                        LineAwesomeIcons.sort_alphabetical_down,
                                        size: 16,
                                        color: UiConstants.primaryGrey,
                                      )
                                    ],
                                  )),
                              SizedBox(
                                  width: 224,
                                  height: 16,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: const [
                                      Text('Commandes',
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: UiConstants.primaryGrey,
                                          )),
                                      Icon(
                                        LineAwesomeIcons.sort_alphabetical_down,
                                        size: 16,
                                        color: UiConstants.primaryGrey,
                                      )
                                    ],
                                  ))
                            ],
                          ),
                        ),
                        Container(
                          height: deviceHeight - 330,
                          padding: const EdgeInsets.fromLTRB(26, 22, 26, 0),
                          child: GridView.builder(
                              controller: gridViewController,
                              gridDelegate:
                                  const SliverGridDelegateWithMaxCrossAxisExtent(
                                      maxCrossAxisExtent: 265,
                                      childAspectRatio: 0.82,
                                      crossAxisSpacing: 14,
                                      mainAxisSpacing: 14),
                              itemCount: 10,
                              itemBuilder: (BuildContext context, index) {
                                return Container(
                                  alignment: Alignment.center,
                                  child: Stack(
                                    children: [
                                      Positioned(
                                        right: 14,
                                        top: 17,
                                        child: SizedBox(
                                          height: 36,
                                          width: 36,
                                          child: OutlinedButton(
                                            onPressed: () =>
                                                print('test $index'),
                                            child: Icon(
                                                LineAwesomeIcons
                                                    .horizontal_ellipsis,
                                                size: 16,
                                                color: UiConstants.primaryGrey),
                                            style: OutlinedButton.styleFrom(
                                                padding: EdgeInsets.zero,
                                                alignment: Alignment.center,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8))),
                                          ),
                                        ),
                                      ),
                                      Column(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      0, 22, 0, 22),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Container(
                                                    width: 64,
                                                    height: 64,
                                                    decoration:
                                                        const BoxDecoration(
                                                      color: UiConstants
                                                          .backgroundLight,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  6)),
                                                    ),
                                                    child: Center(
                                                        child: Text('1a',
                                                            style: UiConstants
                                                                .h1Bold)),
                                                  ),
                                                  Text('Nom du Client',
                                                      style:
                                                          UiConstants.h5Bold),
                                                  Text('adresse du client',
                                                      style: UiConstants
                                                          .regularText14Grey),
                                                  Container(
                                                      width: 152,
                                                      height: 36,
                                                      decoration:
                                                          const BoxDecoration(
                                                        color: UiConstants
                                                            .backgroundLight,
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    8)),
                                                      ),
                                                      child: Center(
                                                        child: Text(
                                                            'Due le 28 fev. 2019',
                                                            style: UiConstants
                                                                .regularTextBold14Grey),
                                                      )),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Divider(
                                            color: UiConstants.backgroundLight,
                                          ),
                                          SizedBox(
                                            height: 78,
                                            child: Row(
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Expanded(
                                                    flex: 1,
                                                    child: Container(
                                                        child: Center(
                                                      child: Text(
                                                          "des trucs\r\net d'autres"),
                                                    ))),
                                                VerticalDivider(
                                                  color: UiConstants
                                                      .backgroundLight,
                                                ),
                                                Expanded(
                                                    flex: 1,
                                                    child: Container(
                                                        child: Center(
                                                      child: Text(
                                                          "des trucs\r\net d'autres"),
                                                    )))
                                              ],
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: UiConstants.outlineResting,
                                          width: 1,
                                          style: BorderStyle.solid),
                                      borderRadius: BorderRadius.circular(12)),
                                );
                              }),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(26, 18, 60, 18),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: 178,
                                height: 30,
                                color: UiConstants.backgroundLight
                                    .withOpacity(0.4),
                              ),
                              Container(
                                width: 360,
                                height: 30,
                                color: UiConstants.backgroundLight
                                    .withOpacity(0.4),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => print('create client'),
        tooltip: 'Créer un client',
        child: const Icon(
          LineAwesomeIcons.plus_circle,
          color: Colors.blueAccent,
        ),
        backgroundColor: Colors.white,
      ),
    );
  }

  Widget _getStatList(
    BuildContext context,
    int index,
  ) {
    double deviceWidth = MediaQuery.of(context).size.width;
    return Container(
      margin: const EdgeInsets.only(bottom: 18),
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 18),
      height: 90,
      decoration: BoxDecoration(
        color: UiConstants.backgroundLight.withOpacity(0.4),
        borderRadius: const BorderRadius.all(Radius.circular(12)),
      ),
      child: Obx(
        () => Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      controller.getDatas(index)[0],
                      style: UiConstants.h5Bold,
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(controller.getDatas(index)[1],
                        style: UiConstants.regularText14Grey),
                  ],
                ),
                Row(children: [
                  Text(
                    controller.getDatas(index)[2][1]
                        ? controller.getDatas(index)[2][0].toString() + ' €'
                        : controller.getDatas(index)[2][0].toString(),
                    style: UiConstants.h3Bold,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Icon(controller.getDatas(index)[3][0],
                      size: 18, color: controller.getDatas(index)[3][1])
                ]),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Stack(
                children: [
                  Container(
                    width: deviceWidth * 0.25 -
                        (UiConstants.leftMenuSize / 2) -
                        100,
                    height: 4,
                    decoration: const BoxDecoration(
                      color: UiConstants.background,
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                  ),
                  Container(
                    width: controller.getDatas(index)[4][0] *
                        (deviceWidth * 0.25 -
                            (UiConstants.leftMenuSize / 2) -
                            72),
                    height: 4,
                    decoration: BoxDecoration(
                      color: controller.getDatas(index)[4][1],
                      borderRadius: const BorderRadius.all(Radius.circular(4)),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
