import 'package:facture_by_ge/Bloc/project/project_view_controller.dart';
import 'package:facture_by_ge/Screen/Layout/skeleton.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

class ProjectView extends StatelessWidget {
  final ProjectViewController controller = Get.put(ProjectViewController());

  ProjectView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MainSkeleton(
      currentIndex: 2,
      body: TextButton(onPressed: () => null, child: const Text('Project')),
      floatingActionButton: FloatingActionButton(
        onPressed: () => print('create project'),
        tooltip: 'Créer un projet',
        child: const Icon(
          LineAwesomeIcons.plus_circle,
          color: Colors.redAccent,
        ),
        backgroundColor: Colors.black,
      ),
    );
  }
}
